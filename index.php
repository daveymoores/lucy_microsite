<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Win with Lucy the movie and IGN</title>


    <meta property="og:title" content="Win with Lucy the movie and IGN">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="Win with Lucy the movie and IGN">
    <meta property="og:url" content="http://uk-microsites.ign.com/lucymovie/">
    <meta property="og:description" content="Win a Toshiba 50 inch TV, a PS4 and a copy of the film">
    <meta property="og:image" content="">
    <meta property="fb:admins" content="546507370">

    <link rel="shortcut icon" href="favicon.ico">
    <link rel=apple-touch-icon href="apple-touch-icon.png">
    <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
    <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">


    <link rel="stylesheet" href="stylesheets/slick.css" />
    <link rel="stylesheet" href="stylesheets/app.css" />

    <script src="bower_components/modernizr/modernizr.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="//use.typekit.net/nwm4qph.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript" src="//cdn.sublimevideo.net/js/g4mzcsac.js"></script>


    <script src="js/hide_it.js"></script>


  </head>


<body>


 <script src="js/output_age_form.js"></script>

<!--uncomment for live-->
<div id="policyNotice" style="display:none">
    <div class="close-btn">
        <a href="#_" title="Close" data-domain=".ign.com">Close</a>
    </div>
    <p>We use cookies and other technologies to improve your online experience. By using this Site, you consent to this use as described in our <a href="http://corp.ign.com/policies/cookie-eu" target="_blank">Cookie Policy</a>.</p>
</div>
<!--uncomment for live-->

    <!--off canvas menu-->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
      <nav class="tab-bar top" data-topbar role="navigation">

          <section class="left-small show-for-small-only">
              <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
          </section>

          <div class="row">

            <div class="ign_red-wrapper ">
              <h1 class="title"><a href="http://ign.com"><img src="img/ign-logo.png" alt="IGN logo"></a></h1>
            </div>

            <ul class="left share_btn" id="share_override">
              <li><a href="#"></a></li>
            </ul>

          </div>

      </nav>

      <aside class="left-off-canvas-menu show-for-small-only">
        <ul class="off-canvas-list" id="offcanvas">
              <li><label>Lucy the movie</label></li>
              <li><a href="#_" class="play">Play the game</a></li>
              <li><a href="#_" class="watch">Watch the trailer</a></li>
              <li><a href="http://bit.ly/LUCY15" target="_blank" class="preorder" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">Pre-order Now</a></li>
        </ul>
      </aside>
      <!--end off canvas menu-->

      <section class="hero" id="main">
        <div class="row">
          <div class="large-12 columns">
            <div class="row">
              
              <div class="large-offset-2 large-8 medium-offset-3 medium-7 columns end">
                <div class="row top_bar">
                  <div class="large-4 medium-4 small-5 columns">
                    <div class="leftBox">
                      <img src="img/lucy_logo.png">
                    </div>
                  </div>
                  <div class="large-3 medium-3 small-4 end columns" id="packshots">
                    <img src="img/packshots.png">
                  </div>
                  <div class="large-5 medium-5 small-9 end columns">
                    <div class="rightBox">
                      <img src="img/preorder_text2.png" alt="#LucyMovie - Out Jan 12th on Blu-Ray and DVD">
                      <a href="http://bit.ly/LUCY15" target="_blank" class="button" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click', 'pre-order in navigation']);">PRE-ORDER NOW</a>
                    </div>
                  </div>  
                </div>
              </div>

            </div>
            <div class="row">

              <div class="large-2 medium-3 columns">
                <ul id="options" class="hide-for-small">
                  <li><a id="play" class="active" href="#_">Play the game</a></li>
                  <li><a id="watch" href="#_">watch the trailer</a></li>
                </ul>
              </div>


                <div class="large-8 medium-7 columns end">
                  <div id="game_container">

                    <div id="moveContainer"><!--move continer-->

                    <div class="flex-video" id="moveAnchor">
                      <iframe src="http://lucy-game.eye-dserve.co.uk" height="490"></iframe>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                      <div class="large-offset-1 large-10 columns end">

                        <div class="row actors">
                          <div class="large-6 medium-6 small-6 columns">scarlett<span class="highlight">johansson</span></div>
                          <div class="large-6 medium-6 small-6 columns">morgan<span class="highlight">freeman</span></div>
                        </div>

                        <div class="clearfix"></div>
                        
                        <div class="row">
                          <div class="large-12 columns">
                            <div class="flex-video youtube">
<!--                               <iframe width="100%" height="260" src="//www.youtube.com/embed/oed3WLnhF6c?rel=0" frameborder="0" allowfullscreen></iframe>                        </div>
 -->                          <video id="6932b99d" class="sublime" width="640" height="360" data-uid="6932b99d" data-autoresize="fit" preload="auto">
                                <source src="http://microsites.ign.com/uk/gopagoda/Lucy20_BDandDVD_Amazon.mp4" />
                                <source src="http://uk-microsites.ign.com/lucy/Lucy20_BDandDVD_Amazon.webm" />
                              </video>

                            </div>
                            <img src="img/movie_stars.png" class="stars" alt="">
                          </div>
                        </div>

                      </div><!--move container-->

                     </div>
                    </div>

                  </div>
                </div>

          </div>
        </div>
      </section>
   

        <footer class="tab-bar">
          <div class="row">

            <div class="ign_red-wrapper">
              <div class="title"><img src="img/ign_ent.png" alt="IGN Entertainment logo"></div>

              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>

            <ul class="left links">

                <li><p>Copyright 2014<br>IGN Entertainment UK, Inc.</p></li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
     
            </ul>

          </div>
        </footer>

      <a class="exit-off-canvas"></a>

    </div>
  </div>


<div id="start_modal" class="reveal-modal small">
    <p>Please confirm you are 18 years of age or over, if you are not please <a href="http://ign.com">go back to IGN</a></p>
</div>


    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>

    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/share.min.js"></script>
    <script src="js/ageChecker.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/waypoints-sticky.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/velocity.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/vote.js"></script>

    <script>

      (function(){
          var cookieName = 'persist-policy';
          var cookieValue = 'eu';
          var createCookie = function(name,value,days,domain) {
              var expires = '';
              var verifiedDomain = '';
              if (days) {
                  var date = new Date();
                  date.setTime(date.getTime()+(days*24*60*60*1000));
                  expires = '; expires='+date.toGMTString();
              }
              if (domain) {
                  verifiedDomain = '; domain='+domain;
              }
              document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
          };
          var readCookie = function(name) {
              var nameEQ = name + "=";
              var ca = document.cookie.split(';');
              for(var i=0;i < ca.length;i++) {
                  var c = ca[i];
                  while (c.charAt(0)==' ') c = c.substring(1,c.length);
                  if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
              }
              return null;
          };

          window.initPolicyWidget = function(){
              jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                  createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                  jQuery('#policyNotice').remove();
                  return false;
              });
          }
          var cookieContent = readCookie(cookieName);
          if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
              jQuery(document).ready(initPolicyWidget);
              jQuery(document).trigger('policyWidgetReady');
          }
      })();

  </script>


  <!--begin GA script-->
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'lucy_tracking']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
  <!--end GA script-->

  <!-- Begin comScore Tag -->
  <!--script>
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3000068" });
    (function() {
      var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
      s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
      el.parentNode.insertBefore(s, el);
    })();
  </script>
  <noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
  </noscript-->
   <!-- End comScore Tag -->

  </body>
</html>
