// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){



	//
	$('#options').on('click', 'a', function(){

		var vidz = $('#game_container'),
			vidzvid = vidz.find('iframe');

		$('#options').find('.active').removeClass('active');
		$(this).addClass('active');

		if($(this).attr('id') == 'watch') {
			
			var vidHeight = vidzvid.height() + 120;

			$('#moveContainer').velocity({
				marginTop : '-' + vidHeight
			}, 450, 'ease-in-out');

			sublime.ready(function(){
			  	var player = sublime.player('6932b99d');
			  	player.play();
			});

		} else {

			$('#moveContainer').velocity({
				marginTop : '0'
			}, 450, 'ease-in-out');

			sublime.ready(function(){
			  	var player = sublime.player('6932b99d');
			  	player.pause();
			});


		}

	});


	//offcanvas moving links
	$('#offcanvas').on('click', 'a', function(){

		var vidz = $('#game_container'),
			vidzvid = vidz.find('iframe');

		if($(this).attr('class') == 'watch') {
			
			var vidHeight = vidzvid.height() + 120;

			$('#moveContainer').velocity({
				marginTop : '-' + vidHeight
			}, 250, 'ease-in-out');

			$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

			sublime.ready(function(){
			  	var player = sublime.player('6932b99d');
			  	player.play();
			});

		} else {

			$('#moveContainer').velocity({
				marginTop : '0'
			}, 250, 'ease-in-out');

			$('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');

			sublime.ready(function(){
			  	var player = sublime.player('6932b99d');
			  	player.pause();
			});

		}

	});

	
	$('.menu-icon').on('click', function(){
		$('.off-canvas-wrap').foundation('offcanvas', 'toggle', 'move-right');
	});


	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}



	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/lucy/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/lucy/",
		      description:    "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with #LucyMovie and @IGNUK"
		    },
		    facebook: {
		      enabled: true,
		      load_sdk: true,// Load the FB SDK. If false, it will default to Facebook's sharer.php implementation. 
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "http://uk-microsites.ign.com/lucy/",
		      //app_id: "551643344962507",
		      title: "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with Lucy and IGN!",
		      caption: "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with #lucymovie and @IGNUK",
		      description:    "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with #lucymovie and @IGNUK"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/lucy/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with #lucymovie and @IGNUK"
		    },
		    email: {
		      enabled: true,
		      title:     "http://uk-microsites.ign.com/lucy/",
		      description:    "Win a Toshiba 50 inch TV, a PS4 and a copy of the film with #lucymovie and @IGNUK"
		    }
		  }

	});
});

