$(document).ready(function(){
    
    $('#submit').click(function(){

        $(this).prop('disabled', true);

        var request = $.ajax({
            url: 'http://evilwithin.jakepyne.net',
            data: { "movie": $('.selected').data('title') },
            type: 'PUT'
        });
    
        request.done(function(){
            $('#submit').submit();

            $('#confirm_modal').foundation('reveal', 'open');
        });
    
        request.fail(function(jqXHR, textStatus) {
            console.log("Voting failed: " + textStatus);
        });
    
        return false;

    });

});