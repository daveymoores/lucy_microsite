<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>The Evil Within - Vote for your favourite horror moive in association with IGN</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link rel=apple-touch-icon href="apple-touch-icon.png">
    <link rel=apple-touch-icon sizes=72x72 href="apple-touch-icon-72x72.png">
    <link rel=apple-touch-icon sizes=114x114 href="apple-touch-icon-114x114.png">

    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="bower_components/modernizr/modernizr.js"></script>
     <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
     <script type="text/javascript" src="//use.typekit.net/nwm4qph.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

  </head>

  <script src="js/hide_it.js"></script>

<body class="thankyou">



    <!--off canvas menu-->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
      <nav class="tab-bar top" data-topbar role="navigation">

          <section class="left-small show-for-small-only">
              <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
          </section>

          <div class="row">

            <div class="ign_red-wrapper ">
              <h1 class="title"><img src="img/ign-logo.png" alt="IGN logo"></h1>
            </div>

            <ul class="left share_btn" id="share_override">
              <li><a href="#"></a></li>
            </ul>

          </div>

      </nav>

      <aside class="left-off-canvas-menu show-for-small-only">
        <ul class="off-canvas-list">
          <li><label>The Evil Within</label></li>
          <li><a href="#">Choose your film</a></li>
          <li><a href="#">Join the screening</a></li>
        </ul>
      </aside>
      <!--end off canvas menu-->


      
        <section class="comp" id="comp">
          <div class="comp_wrapper row">
           <div class="large-8 large-centered thanksWrapper columns">
           	<h2>Thanks for voting</h2>
           	<a href="http://ign.com" class="button">Return to IGN</a>
           </div>
          </div>
        </section>



        <!--start logo garden-->
        <div class="logo_garden">
          <div class="row">
            <div class="large-12">
              <img src="img/logo_garden.jpg" alt="logos">
              <p class="sml_text">© 2014 ZeniMax Media Inc. Developed in association with Tango Gameworks. The Evil Within, Tango, Tango Gameworks, Bethesda, Bethesda Softworks, ZeniMax and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the U.S. and/or other countries. All other trademarks or trade names are the property of their respective owners. All Rights Reserved.</p>
            </div>
          </div>
        </div>
        <!--end logo garden-->

        <footer class="tab-bar">
          <div class="row">

            <div class="ign_red-wrapper">
              <div class="title"><img src="img/ign_ent.png" alt="IGN Entertainment logo"></div>

              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>

            <ul class="left links">
              <ul>
                <li><p>Copyright 2014<br>IGN Entertainment UK, Inc.</p></li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
              </ul>
            </ul>

          </div>
        </footer>

      <a class="exit-off-canvas"></a>

    </div>
  </div>


    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.min.js"></script>

    <script src="bower_components/foundation/js/foundation.min.js"></script>
    <script src="js/share.min.js"></script>
    <script src="js/ageChecker.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/waypoints-sticky.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/vote.js"></script>


  <!--begin GA script-->
  <!--script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-15279170-1']);
    _gaq.push(['_trackPageview', 'CAMPAIGN_NAME']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script-->
  <!--end GA script-->

  <!-- Begin comScore Tag -->
  <!--script>
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3000068" });
    (function() {
      var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
      s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
      el.parentNode.insertBefore(s, el);
    })();
  </script>
  <noscript>
    <img src="http://b.scorecardresearch.com/p?c1=2&c2=3000068&cv=2.0&cj=1" />
  </noscript-->
   <!-- End comScore Tag -->

  </body>
</html>
